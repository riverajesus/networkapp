import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-blue',
  templateUrl: 'blue.html'
})
export class BluetoothPage {

  constructor(public navCtrl: NavController) {

  }

}
