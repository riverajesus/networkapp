import { Component } from '@angular/core';

import { BluetoothPage } from '../blue/blue';
import { WifiPage } from '../wifi/wifi';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = WifiPage;
  tab2Root = BluetoothPage;

  constructor() {

  }
}
