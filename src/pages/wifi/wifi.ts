import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Hotspot, HotspotNetwork } from '@ionic-native/hotspot';

@Component({
  selector: 'page-wifi',
  templateUrl: 'wifi.html'
})
export class WifiPage {

  pageName = 'Wifi'
  networkList = [];
  netInfo;
  isWifiSuported = false;
  isWifiOn = false;
  
  constructor(public navCtrl: NavController, private hotspot: Hotspot) {
    this.hotspot.scanWifi().then((networks: Array<HotspotNetwork>) => {
      this.networkList = networks;  
      console.log('SHOW_NET',networks);
    });
    this.hotspot.isWifiOn().then(isIt => this.isWifiOn = isIt)
    this.hotspot.isWifiSupported().then(isIt => this.isWifiSuported = isIt)
    this.hotspot.getConnectionInfo().then(info => this.netInfo = info)
  }
  // constructor(public navCtrl: NavController){

  // }

}
